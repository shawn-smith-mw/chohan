from abc import abstractmethod
class GameReader:
    @abstractmethod
    def parse_input(self, input) -> list:
        pass