from game_reader import GameReader
from play import Play

class JsonGameReader(GameReader):
    
    def __init__(self):
        pass

    def parse_input(self, input) -> list:
        for item in input:
            item['play'] = read_play(item['play'])
        return input

def read_play(item) -> Play:
    if item.lower() == "cho":
        return Play.Cho
    if item.lower() == "han":
        return Play.Han