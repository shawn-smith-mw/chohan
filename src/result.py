import json
class Result:
    result = []
    dice = -1
    def __init__(self):
        pass

    def __repr__(self) -> str:
        string = "dice: " + str(self.dice) + "\nresult: " + json.dumps(self.result)
        return string
    
    def add_to_result(self,item):
        self.result.append(item)

    def set_dice(self, i):
        self.dice = i
        self.result = []