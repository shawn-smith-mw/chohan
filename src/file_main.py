from logging import exception
from player import Player
from invalidInputException import InvalidInputException
from file_game_reader import FileGameReader
from game import Game
import time

file_name = "file.txt"
game_reader = FileGameReader()

def main():
    f = open(file_name, "r+")
    input = game_reader.parse_input(f)
    players = []
    for i,item in enumerate(input):
        players.append(Player(item, i))
    
    while check_all_above_zero(players):
        f = open(file_name, "r+")
        input = game_reader.parse_input(f)
        for player in players:
            player.place_bet(input)

        game = Game()
        output = game.play(input)
        print("dice roll: ", output.dice)
        for player in players:
            player.collect_results(output.result)
        

        print(players)
        #time.sleep(1)

def check_all_above_zero(results) -> bool:
    for result in results:
        if result.returns <= 0:
            return False
    return True

if __name__ == "__main__":
    main()