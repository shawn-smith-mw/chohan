from player import Player
from game import Game
from flask import Flask, request, jsonify, Response
from invalidInputException import InvalidInputException
import logging
from json_game_reader import JsonGameReader

app = Flask(__name__)
app.logger.setLevel(logging.INFO)

players = []

game_reader = JsonGameReader()

@app.route("/reset")
def reset():
    players.clear()
    return Response(status=200)

@app.route("/", methods=['GET', 'POST'])
def make_bets():
    data = request.json
    data = game_reader.parse_input(data)
    for d in data:
        exists = False
        for player in players:
            if d['name'] == player.name:
                exists = True
        if not exists:
            if len(players) > 0:
                players.append(Player(data[players[-1].place+1], players[-1].place+1))
            else:
                players.append(Player(data[0], 0))
    for player in players:
        player.place_bet(data)

    game = Game()
    try:
        output = game.play(data)
    except InvalidInputException as e:
        return Response(status=400)
    for player in players:
        player.buy_in_if_broke()
        player.collect_results(output.result)
            
    

    return jsonify([x.serialize() for x in players])

if __name__ == '__main__':
    app.run(debug=True)