class Player:

    def __init__(self, input, count):
        self.name = input['name']
        self.returns = 1000
        self.place = count
        self.reset = False
        self.buy_ins = 0
    
    def place_bet(self, input):
        self.returns -= input[self.place]['bet']
    
    def collect_results(self, output):
        self.returns += output[self.place]['returns']
    
    def buy_in_if_broke(self):
        if self.returns <= 0:
            self.buy_ins += 1
            self.returns = 1000
    
    def __repr__(self) -> str:
        return "<Player returns: " + str(self.returns) + ">"
    
    def serialize(self):
        return {
            "name": self.name,
            "returns": self.returns,
            "buy-ins": self.buy_ins
        }