from invalidInputException import InvalidInputException
from play import Play
from result import Result
import random
from logging import Logger

class Game:
    logger = Logger("game.Game")

    def __init__(self):
        pass

    def play(self, input) -> Result:
        result = Result()
        validate_input(input)
        res = random.randint(1,6)
        result.set_dice(res)
        winner = Play.Han
        if res % 2 == 0:
            winner = Play.Cho
        for item in input:
            result.add_to_result(fill_item(item, winner))
        return result
            

def validate_input(input):
    total = 0
    for i in input:
        if i['play'] == Play.Han:
            total = total + i['bet']
        if i['play'] == Play.Cho:
            total = total - i['bet']
    if total != 0:
        raise InvalidInputException

def fill_item(i, winner):
    item = i
    item['won'] = False
    item['returns'] = 0
    if item['play'] == winner:
        item['won'] = True
        item['returns'] = item['bet'] * 2
    del item['play']
    del item['bet']
    return item