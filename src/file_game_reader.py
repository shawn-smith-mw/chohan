from game_reader import GameReader
from invalidInputException import InvalidInputException
from play import Play


class FileGameReader(GameReader):
    
    def __init__(self):
        pass

    def parse_input(self, input) -> list:
        items = []
        for x in input:
            item = x.split(",")
            dic = {}
            if (len(item) == 3):
                dic['name'] = item[0]
                dic['play'] = read_play(item[1])
                dic['bet'] = int(item[2])
            else:
                raise InvalidInputException
            items.append(dic)
        return items

def read_play(item) -> Play:
    if item.lower() == "cho":
        return Play.Cho
    if item.lower() == "han":
        return Play.Han
